package esxihost

import (
	"context"
	"fmt"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/vmware/govmomi/property"
	"github.com/vmware/govmomi/view"
	"github.com/vmware/govmomi/vim25"
	"github.com/vmware/govmomi/vim25/mo"
	"log"
)
var (
	esxiCpuCapacity = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "esxi_cpu_capacity",
		Help: "cpu Capacity MHZ",
	}, []string{"esxi"},
	)
	esxiCpuUsage = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "esxi_cpu_usage",
		Help: "esxi cpu usage MHZ",
	}, []string{"esxi"},
	)
	esxiMemCapacity = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "esxi_mem_capacity",
		Help: "esxi 内存最大 G",
	}, []string{"esxi"},
	)
	esxiMemUsage = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "esxi_mem_usage",
		Help: "esxi 内存使用 G",
	}, []string{"esxi"},
	)
	esxidsCapacity = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "esxi_disk_capacity",
		Help: "esxi 存储最大值 G",
	}, []string{"esxi","esxidisk"},
	)
	esxidsFreeSpace = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "esxi_disk_FreeSpace",
		Help: "esxi 存储未使用 G",
	}, []string{"esxi","esxidisk"},
	)
	esxiUpTime = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "esxi_UpTime",
		Help: "esxi 运行时间",
	}, []string{"esxi","esxiUptime"},
	)
	esxiVms = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "esxi_vms",
		Help: "esxi 虚拟机数量",
	}, []string{"esxi","esxivms"},
	)
)
func init() {
	// Metrics have to be registered to be exposed:
	prometheus.MustRegister(esxiCpuCapacity)
	prometheus.MustRegister(esxiCpuUsage)
	prometheus.MustRegister(esxiMemCapacity)
	prometheus.MustRegister(esxiMemUsage)
	prometheus.MustRegister(esxidsCapacity)
	prometheus.MustRegister(esxidsFreeSpace)
	prometheus.MustRegister(esxiUpTime)
	prometheus.MustRegister(esxiVms)
}

func GetEsxiInfo(c *vim25.Client) error {
	//c := vcconnect.Vccon().Client
	ctx := context.Background()
	m := view.NewManager(c)
	v, err := m.CreateContainerView(ctx, c.ServiceContent.RootFolder, []string{"HostSystem"}, true)
	if err != nil {
		log.Fatal(err)
	}
	defer v.Destroy(ctx)
	// Retrieve summary property for all machines
	// Reference: https://vdc-repo.vmware.com/vmwb-repository/dcr-public/98d63b35-d822-47fe-a87a-ddefd469df06/2e3c7b58-f2bd-486e-8bb1-a75eb0640bee/doc/vim.HostSystem.html
	var hss []mo.HostSystem
	err = v.Retrieve(ctx, []string{"HostSystem"}, []string{"summary", "vm" , "datastore"}, &hss)
	if err != nil {
		log.Fatal(err)
	}

	for _,hs := range hss{
		var esxiHostName = hs.Summary.Config.Name
		//esxi最大cpu
		esxiCpuCapacity.With(prometheus.Labels{"esxi":esxiHostName}).Set(float64(int64(hs.Summary.Hardware.CpuMhz) * int64(hs.Summary.Hardware.NumCpuCores)))
		//esxi当前使用cpu
		esxiCpuUsage.With(prometheus.Labels{"esxi":esxiHostName}).Set(float64(hs.Summary.QuickStats.OverallCpuUsage))
		//esxi最大内存
		esxiMemCapacity.With(prometheus.Labels{"esxi":esxiHostName}).Set(float64(hs.Summary.Hardware.MemorySize / (1024 * 1024)))
		//esxi当前使用内存
		esxiMemUsage.With(prometheus.Labels{"esxi":esxiHostName}).Set(float64(hs.Summary.QuickStats.OverallMemoryUsage))
		//esxi运行时间
		esxiUpTime.With(prometheus.Labels{"esxi":esxiHostName,"esxiUptime":"uptime"}).Set(float64(hs.Summary.QuickStats.Uptime))
        //定义默认连接
		pc := property.DefaultCollector(c)
		//获取ESXI的磁盘信息，先转换类型，hs.Datastore转船成mo.Datastore{}
		var hdss []mo.Datastore
		err := pc.Retrieve(ctx, hs.Datastore, []string{"summary"}, &hdss)
		if err != nil {
			fmt.Println(esxiHostName + "  get esxi datastore info error ", err)
		}else {
			//ESXI有多个的存储
			for _, ds := range hdss {
				//ESXI磁盘总大小
				esxidsCapacity.With(prometheus.Labels{"esxi":esxiHostName,"esxidisk":ds.Summary.Name}).Set(float64(ds.Summary.Capacity / 1024 / 1024 / 1024))
				//未使用空间
				esxidsFreeSpace.With(prometheus.Labels{"esxi":esxiHostName,"esxidisk":ds.Summary.Name}).Set(float64(ds.Summary.FreeSpace / 1024/ 1024 / 1024))
			}
		}
		//每个ESXI的虚拟机数量
		var hdvms []mo.VirtualMachine
		err1 := pc.Retrieve(ctx, hs.Vm, []string{"summary"}, &hdvms)
		if err1 != nil {
			fmt.Println(esxiHostName + "  get esxi VM info error ", err)
		}else {
			esxiVms.With(prometheus.Labels{"esxi":esxiHostName,"esxivms":"exsivms"}).Set(float64(len(hdvms)))
		}
	}
	return nil
}